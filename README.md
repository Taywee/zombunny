# zombunny

Zombie rabbit life sim, with breeding, inbreeding, death, zombification, etc.

Based on my old dead [Zombunny C++ project](https://gitgud.io/Taywee/Zombunny), which was also at one point [changed to use plain OpenGL](https://gitgud.io/Taywee/ZombunnyGL).