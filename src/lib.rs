mod main_menu;
mod simulation;

use bevy::prelude::*;

#[derive(States, Default, Debug, PartialEq, PartialOrd, Ord, Eq, Hash, Clone, Copy)]
pub enum AppState {
    //MainMenu,
    #[default]
    Simulation,
}

pub struct Zombunny;

impl Plugin for Zombunny {
    fn build(&self, app: &mut App) {
        app.add_state::<AppState>()
            .add_plugin(simulation::SimulationPlugin);
    }
}
