use crate::AppState;
use bevy::ecs::query::QueryEntityError;
use bevy::ecs::system::QueryComponentError;
use bevy::math::Vec3Swizzles;
use bevy::prelude::*;
use bevy::render::camera::ScalingMode;
use bevy::utils::Duration;
use bevy::{prelude::*, sprite::MaterialMesh2dBundle};
use bevy_rapier2d::prelude::*;
use bevy_rapier2d::rapier::prelude::CollisionEventFlags;
use rand::{thread_rng, Rng};
use smallvec::SmallVec;
use std::f32::consts::TAU;

fn setup_graphics(mut commands: Commands) {
    commands.spawn(Camera2dBundle {
        projection: OrthographicProjection {
            scale: 1.0 / 30.0,
            scaling_mode: ScalingMode::AutoMin {
                min_width: 1500.0,
                min_height: 1500.0,
            },
            ..default()
        },
        ..default()
    });
}

fn setup_simulation(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    let mut rng = thread_rng();
    // Spawn 10 random rabbits
    for _ in 0..10 {
        let x = rng.gen_range(-10.0..10.0);
        let y = rng.gen_range(-10.0..10.0);
        BunnySpawner {
            transform: Transform::from_xyz(x, y, 0.0),
            ..BunnySpawner::new()
        }
        .spawn(&mut commands, &mut meshes, &mut materials);
    }
    // Spawn walls
    for (x, y) in [(0.0, 1.0), (0.0, -1.0), (1.0, 0.0), (-1.0, 0.0)] {
        commands.spawn((
            Wall,
            SpatialBundle {
                transform: Transform {
                    translation: Vec3 {
                        x: 20.0 * x,
                        y: 20.0 * y,
                        z: 2.0,
                    },
                    ..Default::default()
                },
                ..Default::default()
            },
            RigidBody::Fixed,
            Collider::cuboid(
                if x == 0.0 { 20.0 } else { 1.0 },
                if y == 0.0 { 20.0 } else { 1.0 },
            ),
            ColliderMassProperties::Mass(0.0),
        ));
    }
}

pub struct SimulationPlugin;

#[derive(States, Default, Debug, PartialEq, PartialOrd, Ord, Eq, Hash, Clone, Copy)]
pub enum SimulationState {
    #[default]
    Stopped,
    Running,
}

impl Plugin for SimulationPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(RapierConfiguration {
            gravity: Vect::ZERO,
            ..default()
        })
        .add_state::<SimulationState>()
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::default())
        .add_plugin(RapierDebugRenderPlugin::default())
        .add_systems(
            OnEnter(AppState::Simulation),
            (setup_graphics, setup_simulation, start_simulation),
        )
        .add_systems(OnEnter(SimulationState::Running), start_running)
        .add_systems(OnExit(SimulationState::Running), stop_running)
        .add_systems(
            Update,
            (apply_force, think, gestate, age, handle_collision_events)
                .run_if(in_state(AppState::Simulation)),
            // TODO: Check SimulationState too.
        );
    }
}

#[derive(Component, Copy, Clone, Debug, Default)]
struct Bunny;

#[derive(Component, Copy, Clone, Debug, Default)]
struct Wall;

#[derive(Component, Copy, Clone, Debug)]
struct Zombie;

#[derive(Component, Copy, Clone, Debug, PartialEq, PartialOrd, Ord, Eq, Hash)]
enum Sex {
    Male,
    Female,
}

impl Sex {
    fn random() -> Self {
        if thread_rng().gen::<bool>() {
            Sex::Male
        } else {
            Sex::Female
        }
    }

    fn flip(self) -> Self {
        match self {
            Sex::Male => Sex::Female,
            Sex::Female => Sex::Male,
        }
    }
}

#[derive(Component, Debug)]
struct Pregnant(Timer);

#[derive(Component, Debug)]
struct Young(Timer);

#[derive(Component, Debug, Clone, Copy, PartialEq, PartialOrd, Ord, Eq, Hash)]
struct RabbitSprite;

#[derive(Component, Clone, Debug, Default)]
enum State {
    #[default]
    Idle,
    Wandering(Wandering),
    Pursuing(Vec2),
    Fleeing(Vec2),
}

#[derive(Clone, Debug)]
struct Wandering {
    timer: Timer,
}

const WANDER_FORCE: f32 = 1.0;
const PURSUE_FORCE: f32 = 1.0;
const FLEE_FORCE: f32 = 1.0;
const WANDER_DURATION_SECONDS: f32 = 2.0;
const PREGNANCY_SECONDS: f32 = 5.0;
const YOUNG_SECONDS: f32 = 5.0;
const BUNNY_RADIUS: f32 = 0.25;
const VISION_RADIUS: f32 = 10.0;

const MALE_COLOR: Color = Color::RED;
const FEMALE_COLOR: Color = Color::BLUE;
const PREGNANT_COLOR: Color = Color::PURPLE;
const ZOMBIE_COLOR: Color = Color::DARK_GREEN;
const YOUNG_MALE_COLOR: Color = Color::PINK;
const YOUNG_FEMALE_COLOR: Color = Color::TURQUOISE;

impl Wandering {
    fn random_direction() -> Vec2 {
        Vec2::from_angle(thread_rng().gen_range(0.0..TAU))
    }

    fn tick(&mut self, delta: Duration) -> Option<Vec2> {
        self.timer.tick(delta);
        if self.timer.just_finished() {
            Some(Self::random_direction())
        } else {
            None
        }
    }

    fn new() -> (Self, Vec2) {
        // New wandering hasn't ticked yet, start it off and give a random direction.
        let mut timer = Timer::from_seconds(WANDER_DURATION_SECONDS, TimerMode::Repeating);
        timer.tick(Duration::from_secs_f32(
            thread_rng().gen_range(0.0..WANDER_DURATION_SECONDS),
        ));
        (Self { timer }, Self::random_direction())
    }
}

#[derive(Bundle, Default)]
struct StateBundle {
    pub state: State,
    pub external_force: ExternalForce,
}

#[derive(Copy, Clone, Debug)]
struct BunnySpawner {
    pub transform: Transform,
    pub sex: Sex,
    pub young: bool,
}

impl BunnySpawner {
    pub fn new() -> Self {
        BunnySpawner {
            transform: Transform::default(),
            sex: Sex::random(),
            young: false,
        }
    }

    pub fn spawn(
        self,
        commands: &mut Commands,
        meshes: &mut Assets<Mesh>,
        materials: &mut Assets<ColorMaterial>,
    ) {
        const SOLID_GROUP: Group = Group::GROUP_1;
        const MALE_GROUP: Group = Group::GROUP_2;
        const FEMALE_GROUP: Group = Group::GROUP_3;
        const ZOMBIE_GROUP: Group = Group::GROUP_4;
        const SCARED_GROUP: Group = Group::GROUP_5;
        const SENSOR_GROUP: Group = Group::GROUP_6;

        let mut entity = commands.spawn(SpatialBundle {
            transform: self.transform,
            ..default()
        });
        entity
            .insert((
                ActiveEvents::COLLISION_EVENTS,
                Name::new("rabbit"),
                RigidBody::Dynamic,
                Collider::ball(BUNNY_RADIUS),
                Bunny::default(),
                StateBundle::default(),
                Sleeping::disabled(),
                self.sex,
                Damping {
                    linear_damping: 1.0,
                    ..default()
                },
                Friction {
                    coefficient: 0.0,
                    ..default()
                },
                LockedAxes::ROTATION_LOCKED,
            ))
            .insert((
                CollisionGroups::new(
                    SOLID_GROUP
                        | match self.sex {
                            Sex::Male => MALE_GROUP,
                            Sex::Female => FEMALE_GROUP,
                        },
                    SOLID_GROUP | SENSOR_GROUP,
                ),
                ColliderMassProperties::Mass(2.25),
            ));
        if self.young {
            entity.insert(Young(Timer::from_seconds(YOUNG_SECONDS, TimerMode::Once)));
        }
        entity
            .with_children(|parent| {
                // Add the sprite as a child entity.
                parent.spawn(SpatialBundle::default()).insert((
                    RabbitSprite,
                    MaterialMesh2dBundle {
                        mesh: meshes
                            .add(Mesh::from(shape::Circle {
                                radius: BUNNY_RADIUS,
                                ..default()
                            }))
                            .into(),
                        transform: Transform {
                            translation: Vec3 {
                                x: 0.0,
                                y: 0.0,
                                z: 1.0,
                            },
                            ..default()
                        },
                        material: materials.add(ColorMaterial::from(
                            match (self.sex, self.young) {
                                (Sex::Male, false) => MALE_COLOR,
                                (Sex::Female, false) => FEMALE_COLOR,
                                (Sex::Male, true) => YOUNG_MALE_COLOR,
                                (Sex::Female, true) => YOUNG_FEMALE_COLOR,
                            },
                        )),
                        ..default()
                    },
                ));
            })
            .with_children(|parent| {
                // Add the sensor as a child entity.
                parent
                    .spawn(SpatialBundle::default())
                    .insert((
                        Name::new("RabbitSensor"),
                        Sensor,
                        Collider::ball(VISION_RADIUS),
                        TransformBundle::default(),
                        ColliderMassProperties::Mass(0.0),
                    ))
                    .insert(CollisionGroups::new(
                        SENSOR_GROUP,
                        ZOMBIE_GROUP
                            | SCARED_GROUP
                            | match self.sex {
                                Sex::Male => FEMALE_GROUP,
                                Sex::Female => MALE_GROUP,
                            },
                    ));
            });
    }
}

fn start_simulation(mut next_state: ResMut<NextState<SimulationState>>) {
    next_state.set(SimulationState::Running);
}

fn start_running(mut rapier_config: ResMut<RapierConfiguration>) {
    rapier_config.physics_pipeline_active = true;
    rapier_config.query_pipeline_active = true;
}

fn stop_running(mut rapier_config: ResMut<RapierConfiguration>) {
    rapier_config.physics_pipeline_active = false;
    rapier_config.query_pipeline_active = false;
}

fn apply_force(
    time: Res<Time>,
    mut query: Query<(&mut State, &mut ExternalForce, &GlobalTransform)>,
) {
    let delta = time.delta();
    for (mut state, mut external_force, transform) in &mut query {
        let force = match &mut *state {
            State::Idle => {
                let (wandering, direction) = Wandering::new();
                *state = State::Wandering(wandering);
                Some(direction * WANDER_FORCE)
            }
            State::Wandering(wandering) => wandering.tick(delta).map(|dir| dir * WANDER_FORCE),
            State::Pursuing(target) => {
                Some((*target - transform.translation().xy()).normalize() * PURSUE_FORCE)
            }
            State::Fleeing(target) => {
                Some((transform.translation().xy() - *target).normalize() * FLEE_FORCE)
            }
        };

        if let Some(force) = force {
            external_force.force = force;
        }
    }
}

fn think(
    rapier_context: Res<RapierContext>,
    bunny_query: Query<(Entity, &Sex, Option<&Pregnant>, Option<&Young>), With<Bunny>>,
    other_bunny_query: Query<&Sex, (Without<Pregnant>, Without<Young>, Without<Zombie>)>,
    mut state_query: Query<&mut State>,
    children_query: Query<&Children>,
    sensor_query: Query<Entity, With<Sensor>>,
    transform_query: Query<&GlobalTransform>,
) {
    for (bunny, &sex, pregnant, young) in &bunny_query {
        let bunny_position = transform_query
            .get_component::<GlobalTransform>(bunny)
            .expect("Getting top level bunny position")
            .translation()
            .xy();

        for descendent in children_query.iter_descendants(bunny) {
            if sensor_query.contains(descendent) {
                let sensor = descendent;

                // nearest potential mate position and distance squared
                let mut nearest_potential_mate: Option<(Vec2, f32)> = None;

                // All the zombies
                let mut zombies: SmallVec<[Vec2; 128]> = Default::default();

                for (collider1, collider2, intersecting) in
                    rapier_context.intersections_with(sensor)
                {
                    if intersecting {
                        let other = if collider1 == sensor {
                            collider2
                        } else {
                            collider1
                        };

                        let wants_to_mate = pregnant.is_none() && young.is_none();

                        if wants_to_mate {
                            let other_bunny_sex =
                                other_bunny_query.get_component::<Sex>(other).ok().copied();

                            if other_bunny_sex == Some(sex.flip()) {
                                let other_bunny_position = transform_query
                                    .get_component::<GlobalTransform>(other)
                                    .ok()
                                    .map(|transform| transform.translation().xy());
                                if let Some(other_bunny_position) = other_bunny_position {
                                    match &mut nearest_potential_mate {
                                        None => {
                                            nearest_potential_mate = Some((
                                                other_bunny_position,
                                                bunny_position
                                                    .distance_squared(other_bunny_position),
                                            ));
                                        }
                                        Some((nearest, distance_squared)) => {
                                            let other_distance_squared = bunny_position
                                                .distance_squared(other_bunny_position);
                                            if other_distance_squared < *distance_squared {
                                                *nearest = other_bunny_position;
                                                *distance_squared = other_distance_squared;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                let mut state = state_query
                    .get_component_mut::<State>(bunny)
                    .expect("getting top level bunny state");

                if let Some((target, _)) = nearest_potential_mate {
                    *state = State::Pursuing(target);
                } else {
                    if !matches!(*state, State::Wandering(_)) {
                        *state = State::Idle;
                    }
                }
            }
        }
    }
}

fn gestate(
    mut commands: Commands,
    time: Res<Time>,
    mut query: Query<(Entity, &GlobalTransform, &mut Pregnant, &Children)>,
    rabbit_sprite_query: Query<(), With<RabbitSprite>>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    let delta = time.delta();
    for (entity, transform, mut pregnant, children) in &mut query {
        pregnant.0.tick(delta);
        if pregnant.0.finished() {
            for child in children {
                if rabbit_sprite_query.contains(*child) {
                    commands
                        .entity(*child)
                        .insert(materials.add(ColorMaterial::from(FEMALE_COLOR)));
                    break;
                }
            }
            commands.entity(entity).remove::<Pregnant>();
            let spawn_point = transform.translation().xy()
                + Vec2::from_angle(thread_rng().gen_range(0.0..TAU)) * BUNNY_RADIUS * 2.0;
            let spawner = BunnySpawner {
                transform: Transform {
                    translation: spawn_point.extend(0.0),
                    ..Default::default()
                },
                young: true,
                ..BunnySpawner::new()
            };
            spawner.spawn(&mut commands, &mut meshes, &mut materials);
        }
    }
}

fn age(
    mut commands: Commands,
    time: Res<Time>,
    mut query: Query<(Entity, &Sex, &mut Young, &Children)>,
    rabbit_sprite_query: Query<(), With<RabbitSprite>>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    let delta = time.delta();
    for (entity, sex, mut young, children) in &mut query {
        young.0.tick(delta);
        if young.0.finished() {
            commands.entity(entity).remove::<Young>();
            for child in children {
                if rabbit_sprite_query.contains(*child) {
                    commands
                        .entity(*child)
                        .insert(materials.add(ColorMaterial::from(match sex {
                            Sex::Male => MALE_COLOR,
                            Sex::Female => FEMALE_COLOR,
                        })));
                    break;
                }
            }
        }
    }
}

fn handle_collision_events(
    mut commands: Commands,
    mut events: EventReader<CollisionEvent>,
    bunny_query: Query<
        (
            Entity,
            &Sex,
            Option<&Pregnant>,
            Option<&Zombie>,
            Option<&Young>,
        ),
        With<Bunny>,
    >,
    mut materials: ResMut<Assets<ColorMaterial>>,
    children_query: Query<&Children>,
    mut color_material_query: Query<&mut Handle<ColorMaterial>>,
) {
    for &event in events.iter() {
        if let CollisionEvent::Started(first, second, flags) = event {
            if (flags & (CollisionEventFlags::SENSOR | CollisionEventFlags::REMOVED)).is_empty() {
                let first = bunny_query.get(first).ok();
                let second = bunny_query.get(second).ok();

                match (first, second) {
                    (
                        Some((male, &Sex::Male, None, None, None)),
                        Some((female, &Sex::Female, None, None, None)),
                    )
                    | (
                        Some((female, &Sex::Female, None, None, None)),
                        Some((male, &Sex::Male, None, None, None)),
                    ) => {
                        commands.entity(female).insert(Pregnant(Timer::from_seconds(
                            PREGNANCY_SECONDS,
                            TimerMode::Once,
                        )));
                        for descendent in children_query.iter_descendants(female) {
                            if let Some(mut color_material) =
                                color_material_query.get_mut(descendent).ok()
                            {
                                *color_material =
                                    materials.add(ColorMaterial::from(PREGNANT_COLOR));
                                break;
                            }
                        }
                    }
                    (Some((first, _, _, None, _)), Some((second, _, _, Some(_), _))) => {
                        todo!("Zombify left")
                    }
                    (Some((first, _, _, Some(_), _)), Some((second, _, _, None, _))) => {
                        todo!("Zombify right")
                    }
                    _ => (),
                }
            }
        }
    }
}
